import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Pages/Home'
import Callback from './../components/Pages/Callback'
import Profile from '@/components/Pages/Profile'
import ProfileCompletion from '@/components/Pages/ProfileCompletion/ProfileCompletion'
import Settings from '@/components/Pages/Settings/Settings'
import Chat from '@/components/Pages/Chat'
import firebase from 'firebase'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/callback',
      name: 'Callback',
      component: Callback
    },
    {
      path: '/Profile',
      name: 'Profile',
      component: Profile,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/profile-completion',
      name: 'ProfileCompletion',
      component: ProfileCompletion,
      props: true,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      props: true,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat,
      props: true,
      meta: {
        requiresAuth: true
      }
    }

  ]
})

router.beforeEach((to, from, next) => {
  // Check for requiredAuth guard
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Check if NOT logged in
    if (!firebase.auth().currentUser) {
      // Go to home page
      next({
        path: '/'
      })
    } else {
      // Proceed to the route
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    // Check if logged in
    if (firebase.auth().currentUser) {
      // Go to home page
      next({
        path: '/'
      })
    } else {
      // Proceed to the route
      next()
    }
  } else {
    // Proceed to the route
    next()
  }
})

export default router