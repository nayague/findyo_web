import vue from 'vue'
import axios from 'axios'

function getLeaderBoard() {
  axios.get(`https://localhost:44352/api/LeaderBoard/GetLeaderBoard`)
    .then(response => {
      //console.log(response.data.topCategories)
      return response.data.topCategories
    })
    .catch(e => {
      return undefined
      console.log(e)
    })
}

// user camelCase
// no need to use ; to end statement as it's outdated as per es6
// var leaderBoard = getLeaderBoard()

// console.log(leaderBoard)

export function dummyTopRatedFreelancers() {
  var result
  if (getLeaderBoard()) {
    result = getLeaderBoard()
  }
  return result
}
export function dummyTopCategories() {
  return dummyTopCategories
}
export function dummyTopServices() {
  return dummyTopServices
}
var dummyTopServices =
[
  {
    'serviceId': 1,
    'serviceTitle': 'design 5 photos and a logo',
    'servicePrice': 550,
    'serviceRating': '55.45%',
    'userId': 1,
    'username': 'g.sandamal',
    'firstname': 'gayan',
    'lastname': 'sandamal',
    'userDp': 'user',
  },
  {
    'serviceId': 2,
    'serviceTitle': 'motivate you none to second',
    'servicePrice': 2000,
    'serviceRating': '97.45%',
    'userId': 2,
    'username': 'savindid',
    'firstname': 'savindi',
    'lastname': 'weerakoon',
    'userDp': 'user',
  },
  {
    'serviceId': 3,
    'serviceTitle': 'eat your whole food and recommend',
    'servicePrice': 10,
    'serviceRating': '15.95%',
    'userId': 3,
    'username': 'p.silva',
    'firstname': 'pasindu',
    'lastname': 'silva',
    'title': 'food consultant',
    'userDp': 'user',
  }
]
var dummyTopCategories =
[
  {
    categoryId: 1,
    categoryName: 'photographers',
    categoryItemCount: 201,
    categoryThumbnail: 'camera'
  },
  {
    categoryId: 2,
    categoryName: 'philosophers',
    categoryItemCount: 1,
    categoryThumbnail: 'camera'
  },
  {
    categoryId: 3,
    categoryName: 'plumbers',
    categoryItemCount: 1005,
    categoryThumbnail: 'camera'
  },
  {
    categoryId: 1,
    categoryName: 'photographers',
    categoryItemCount: 201,
    categoryThumbnail: 'camera'
  },
  {
    categoryId: 2,
    categoryName: 'philosophers',
    categoryItemCount: 1,
    categoryThumbnail: 'camera'
  }
]
var dummyTopRatedFreelancers =
[
  {
    'userId': 1,
    'username': 'g.sandamal',
    'firstname': 'gayan',
    'lastname': 'sandamal',
    'title': 'ux engineer',
    'userDp': 'user',
    'rating': '96.66%'
  },
  {
    'userId': 2,
    'username': 'savindid',
    'firstname': 'savindi',
    'lastname': 'weerakoon',
    'title': 'motivator',
    'userDp': 'user',
    'rating': '99.66%'
  },
  {
    'userId': 3,
    'username': 'p.silva',
    'firstname': 'pasindu',
    'lastname': 'silva',
    'title': 'food consultant',
    'userDp': 'user',
    'rating': '02.10%'
  },
  {
    'userId': 1,
    'username': 'g.sandamal',
    'firstname': 'gayan',
    'lastname': 'sandamal',
    'title': 'ux engineer',
    'userDp': 'user',
    'rating': '96.66%'
  },
  {
    'userId': 2,
    'username': 'savindid',
    'firstname': 'savindi',
    'lastname': 'weerakoon',
    'title': 'motivator',
    'userDp': 'user',
    'rating': '99.66%'
  }
]