export function dummyComments() {
    return dummyPostComments
}
var dummyPostComments =
[
    {
      'postId': 1,
      'commentId': 1,
      'userId': 1,
      'username': 'g.sandamal',
      'firstname': 'gayan',
      'lastname': 'sandamal',
      'liked': true,
      'totalLikes': 5,
      'comment': 'Hi, I want your service but the cost is to much. Would you please reduce it?',
      'time': '2018-09-22T17:15:29.251Z',
      'userDp': 'user.jpg',
      'reply': [
        {
          'postId': 1,
          'commentId': 10,
          'userId': 2,
          'username': 'savindid',
          'firstname': 'savindi',
          'lastname': 'weerakoon',
          'liked': true,
          'totalLikes': 10,
          'comment': 'Hey, I have 3 packages',
          'time': '2018-09-15T17:15:29.251Z',
          'userDp': 'user.jpg',
        },
        {
          'postId': 1,
          'commentId': 11,
          'userId': 2,
          'username': 'savindid',
          'firstname': 'savindi',
          'lastname': 'weerakoon',
          'liked': false,
          'totalLikes': 99,
          'comment': '1. shooi. 2. shooooi. and 3. shoooooooi',
          'time': '2018-09-15T17:15:29.251Z',
          'userDp': 'user.jpg',
          'reply': [
            {
              'postId': 1,
              'commentId': 1,
              'userId': 1,
              'username': 'g.sandamal',
              'firstname': 'gayan',
              'lastname': 'sandamal',
              'liked': false,
              'comment': 'Thanks for attractive packages',
              'time': '2018-09-15T17:15:29.251Z',
              'userDp': 'user.jpg',
            }
          ]
        }
      ]
    },
    {
      'postId': 1,
      'commentId': 1,
      'userId': 3,
      'username': 'p.silva',
      'firstname': 'pasindu',
      'lastname': 'silva',
      'liked': false,
      'comment': 'What is this?',
      'time': '2018-09-15T17:23:20.251Z',
      'userDp': 'user.jpg',
    }
]