export function dummyPosts() {
    return dummyPosts
}
var dummyPosts =
[
  {
    'postType': 1,
    'postId': 1,
    'username': 'g.sandamal',
    'firstname':  'gayan',
    'lastname': 'sandamal',
    'userDp': 'user.jpg',
    'time': '2019-01-14T17:15:29.251Z',
    // 'expireTime': '2019-01-20T15:38:40.251Z',
    'liked': true,
    'totalLikes': 5,
    'totalComments': 4,
    'totalShares': 2,
    'saved': true,
    'description': `<span class="big-o">LA</span>
    orem ipsum dolor sit amet, <b>consectetuer</b> adipiscing elit. 
    <i>Aenean</i> commodo ligula eget dolor. Aenean massa. Cum sociis natoque <u>penatibus</u> 
    et magnis dis parturient montes, nascetur ridiculus mus. <div class="all-caps">Donec</div> 
    quam felis, <div class="all-caps"><b>ultricies</b></div> nec. imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
    Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. DONEC quam felis, ULTRICIES nec. imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis`,
    'expiration': '2018-09-15T17:15:29.251Z'
  },
  {
    'postType': 2,
    'postId': 2,
    'userId': 1,
    'username': 'g.sandamal',
    'firstname': 'gayan',
    'lastname': 'sandamal',
    'userDp': 'user.jpg',
    'time': '2018-09-15T17:15:29.251Z',
    'liked': true,
    'totalLikes': 5,
    'totalComments': 4,
    'totalShares': 2,
    'saved': true,
    'description': `<span class="big-o">L</span>
    orem ipsum dolor sit amet, <b>consectetuer</b> adipiscing elit. 
    <i>Aenean</i> commodo ligula eget dolor. Aenean massa. Cum sociis natoque <u>penatibus</u> 
    et magnis dis parturient montes, nascetur ridiculus mus. <div class="all-caps">Donec</div> 
    quam felis, <div class="all-caps"><b>ultricies</b></div> nec. imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
    Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. DONEC quam felis, ULTRICIES nec. imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis`,
    'expiration': '2018-09-15T17:15:29.251Z',
    'slider': [
      {
        'sliderImgId': 1,
        'sliderImgName': 'travel-photographer'
      },
      {
        'sliderImgId': 2,
        'sliderImgName': 'b57b0bd8-0c78-4129-81f6-18410451c4f2'
      },
      {
        'sliderImgId': 3,
        'sliderImgName': 'Travellers-Movie-Wallpapers-1080x675'
      }
    ]
  },
  {
    'postType': 3,
    'postId': 3,
    'userId': 1,
    'username': 'g.sandamal',
    'firstname': 'gayan',
    'lastname': 'sandamal',
    'userDp': 'user.jpg',
    'time': '2018-09-15T17:15:29.251Z',
    'liked': true,
    'totalLikes': 5,
    'totalComments': 4,
    'totalShares': 2,
    'saved': true,
    'serviceTitle': `I will design your website`,
    'description': `<span class="big-o">L</span>
    orem ipsum dolor sit amet, <b>consectetuer</b> adipiscing elit. 
    <i>Aenean</i> commodo ligula eget dolor. Aenean massa. Cum sociis natoque <u>penatibus</u> 
    et magnis dis parturient montes, nascetur ridiculus mus. <div class="all-caps">Donec</div> 
    quam felis, <div class="all-caps"><b>ultricies</b></div> nec. imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis
    Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. DONEC quam felis, ULTRICIES nec. imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis`,
    'servicePrice': 24000,
    'expiration': '2018-09-15T17:15:29.251Z',
    'slider': [
      {
        'sliderImgId': 1,
        'sliderImgName': 'travel-photographer'
      },
      {
        'sliderImgId': 2,
        'sliderImgName': 'b57b0bd8-0c78-4129-81f6-18410451c4f2'
      },
      {
        'sliderImgId': 3,
        'sliderImgName': 'Travellers-Movie-Wallpapers-1080x675'
      }
    ]
  }
]