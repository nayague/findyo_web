export function dummyProfileSummary() {
  return profileSummary
}
var profileSummary =
{
  // profile_picture: 'user.jpg',
  profile_picture: '',
  aboutMe: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
  serviceTitle: 'design 5 photos and a logo',
  endorsements: [
    {
      skill: 'Vue.JS',
      endorsement: 21,
      rate: 4
    },
    {
      skill: 'JavaScript',
      endorsement: 15,
      rate: 5
    },
    {
      skill: 'C#.Net',
      endorsement: 6,
      rate: 2
    },
    {
      skill: 'Photoshop',
      endorsement: 40,
      rate: 4
    }
  ],
  experiences: [
    {
      title: 'UI Engineer',
      employer: 'Platform One Pvt Ltd',
      employerURL: 'https://platform1.net',
      from: '2018-06-11T17:09:29.251Z'
    },
    {
      title: 'UI/UX Engineer',
      employer: 'Suwahas holdings Pvt Ltd',
      employerURL: 'https://suwahas.com',
      from: '2017-09-01T17:15:29.251Z',
      to: '2018-06-11T17:15:29.251Z',
    },
    {
      title: 'UI Engineer(Jr)',
      employer: 'Quard International Pvt Ltd',
      employerURL: 'https://quardinternational.com',
      from: '2016-08-11T17:15:29.251Z',
      to: '2017-08-31T17:15:29.251Z',
    },
    {
      title: 'Web designer (Trainee)',
      employer: '3CS Web Designs Pvt Ltd',
      employerURL: 'https://3cs.com',
      from: '2016-02-01T17:15:29.251Z',
      to: '2016-11-01T17:15:29.251Z',
    },
    {
      title: 'Web Developer (Intern)',
      employer: 'Miami Exports Pvt Ltd',
      employerURL: 'https://bamholdings.com',
      from: '2015-06-01T17:15:29.251Z',
      to: '2015-09-30T17:15:29.251Z',
    }
  ],
  clientReviews: [
    {
      reviewId: 1,
      userId: 5654,
      userName: 'g.sandamal',
      firstname: 'gayan',
      lastname: 'sandamal',
      title: 'Great',
      comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      rate: 4,
      time: '2018-09-22T17:15:29.251Z',
      userImage: 'user.jpg',
    },
    {
      reviewId: 2,
      userId: 3284,
      userName: 'g.sandamal',
      firstname: 'gayan',
      lastname: 'sandamal',
      registeredUser: true,
      title: 'Great',
      comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      rate: 4,
      time: '2018-09-22T17:15:29.251Z',
      userImage: '',
    },
    {
      reviewId: 2,
      userId: 4564,
      userName: 'g.sandamal',
      firstname: 'gayan',
      lastname: 'sandamal',
      registeredUser: true,
      title: 'Great',
      comment: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      rate: 4,
      time: '2018-09-22T17:15:29.251Z'
    }
  ],
  galleryOverview: [
    {
      id: 1,
      title: 'asd3rsfdf',
      imageURL: 'http://findyo.lk/dev/findyo/uploads/sample/gallery-img-1.jpg'
    },
    {
      id: 2,
      title: 'cfge344e',
      imageURL: 'http://findyo.lk/dev/findyo/uploads/sample/gallery-img-2.jpg'
    },
    {
      id: 3,
      title: 'fdge4gxd',
      imageURL: 'http://findyo.lk/dev/findyo/uploads/sample/gallery-img-3.jpg'
    },
    {
      id: 4,
      title: 'fbg5dfg',
      imageURL: 'http://findyo.lk/dev/findyo/uploads/sample/gallery-img-4.jpg'
    },
    {
      id: 5,
      title: 'v45ugdfgdr',
      imageURL: 'http://findyo.lk/dev/findyo/uploads/sample/gallery-img-5.jpg'
    }
  ],
  userTrendings: [
    {
      title: 'Efficiency',
      rating: 4,
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      thumbnail: 'http://findyo.lk/dev/findyo/uploads/sample/project-1.png'
    },
    {
      title: 'HRM',
      rating: 2,
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      thumbnail: 'http://findyo.lk/dev/findyo/uploads/sample/project-2.jpg'
    },
    {
      title: 'Dashboard',
      rating: 5,
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      thumbnail: 'http://findyo.lk/dev/findyo/uploads/sample/project-3.jpg'
    },
    {
      title: 'User Profile',
      rating: 1,
      description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
      thumbnail: 'http://findyo.lk/dev/findyo/uploads/sample/project-1.jpg'
    }
  ],
  bio: {
    coverLetter: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'
  }
}