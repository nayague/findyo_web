// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueCarousel from 'vue-carousel'
import firebase from 'firebase'
import './components/firebaseInit'

Vue.config.productionTip = false
Vue.use(VueCarousel)

let app
firebase.auth().onAuthStateChanged(user=>{
  if(!app){
    app = new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }
})

// var Vue = require('vue')
var VueAutosize = require('vue-autosize')

Vue.use(VueAutosize)
/* eslint-disable no-new */

