import auth0 from 'auth0-js'
import { debug } from 'util';

const webAuth = new auth0.WebAuth({
    domain: 'findyo.au.auth0.com',
    clientID: '7GXvx59oJQgAvqAHP9zFtuuRbyBDcG9d',
    redirectUri: 'http://localhost:8080/callback',
    responseType: 'token id_token',
    scope: 'openid',
    sso: false
})

let tokens = {}

const login = () => {
    webAuth.authorize()
}

const handleAuth = (cb) => {
    webAuth.parseHash((err, authResult) => {
        if (authResult && authResult.accessToken && authResult.idToken) {
            tokens.accessToken = authResult.accessToken
            tokens.idToken = authResult.idToken
            tokens.expiry = (new Date()).getTime() + authResult.expiresIn * 1000
            cb()
        } else {
            console.log(err)
        }
    })
}

const isLoggedIn = () => {
    return (tokens.accessToken && (new Date()).getTime() < tokens.expiry)
}

const token = () => {
    return tokens.accessToken
}

const logout = () => {
    webAuth.logout({
        returnTo: 'http://localhost:8080'
    })
    tokens = {}
}


export {
    login,
    handleAuth,
    isLoggedIn,
    logout,
    token
}